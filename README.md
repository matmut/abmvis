# AbmVis Application
AbmVis is a data visualizer for mobility models produced by simulators like MatSim.

## Project setup guide
Use recommended .gitignore file\
Download Maven dependencies and build with Maven\
Project language level: 11\
Test: JUnit5

DO NOT only add import in your class!\
Otherwise the artifact won't work!\
If your code need an external library, use Maven like this: 
1. Search its maven dependency online
2. Paste it to pom.xml under dependencies
3. Click download dependency in Maven plugin
4. Build and run (w/ Maven)

## Raccourcis clavier temporaires
* U unzoom
* Z zoom
* D avancer du pas
* Q reculer du pas
* A diminuer le pas
* E augmenter le pas
* W diminuer la vitesse
* C augmenter la vitesse
* S lancer la simulation
* P mettre en pause la simulation




