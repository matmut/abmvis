package UIAbmModel;

import AbmModel.Event;
import AbmModel.TrafficEvent;
import AbmModel.Vehicle;
import AbmModel.VehicleEntersLink;
import MatsimModel.MSVehicle;
import javafx.util.Pair;

import java.util.*;

/**
 * Analyze implementation for the number of vehicles in traffic over the time
 */
public class VehiclesInTrafficAnalysis extends Analysis {

    /**
     * Constructor
     * @param uiScenario The current uiScenario to be analyzed
     */
    public VehiclesInTrafficAnalysis(UIScenario uiScenario) {
        super(uiScenario);
        title = "Véhicules en circulation au cours du temps";
        cls = Vehicle.class;
    }

    @Override
    public void computeAnalysis(Optional<Pair<String, String>> type) {
        List<Pair<Double, Double>> result;
        String typeValue = (type.isEmpty()) ? "All" : type.get().getKey() + "/" + type.get().getValue();
        resPerType.put(typeValue, new ArrayList<>());
        result = resPerType.get(typeValue);

        List<RunnableEvent> events = uiScenario.getUievents();
        double amplitude = 0.0;
        double min = 0.0;
        result.add(new Pair(x0, amplitude));

        for(RunnableEvent event : events) {
            if(event instanceof RunnableTrafficEvent && event.getEvent().isValid()) {
                TrafficEvent e = (TrafficEvent) event.getEvent();
                if(type.isEmpty() || e.getVehicleObject().getTypes().getValues(type.get().getKey()).contains(type.get().getValue())) {
                    result.add(new Pair(event.getEvent().getTime(), amplitude));
                    if(event instanceof RunnableVehicleEntersLink) amplitude += 1.0;
                    if(event instanceof RunnableVehicleLeavesLink) amplitude -= 1.0;
                    result.add(new Pair(event.getEvent().getTime(), amplitude));
                    if(amplitude < min) min = amplitude;
                }
            }
        }

        if(!result.isEmpty()) {
            Pair<Double, Double> lastPoint = result.get(result.size() - 1);
            result.add(new Pair(xf, lastPoint.getValue()));
        }

        if(min != 0.0) {
            List<Pair<Double, Double>> update = new ArrayList<>();
            for(Pair<Double, Double> point : result) update.add(new Pair<>(point.getKey(), point.getValue() - min));
            resPerType.put(typeValue, update);
        }

        for(int i = result.size()-2; i>=1; i--){
            Pair<Double,Double> pair2 = result.get(i-1);
            Pair<Double,Double> pair3 = result.get(i);
            Pair<Double,Double> pair4 = result.get(i+1);
            //System.out.println("p1:"+pair2.getKey()+" p2:"+pair3.getKey()+" p3:"+pair4.getKey());
            //System.out.println("p1 == p2:"+(pair2.getKey().doubleValue()==pair3.getKey().doubleValue()));
            //System.out.println("p1 == p3:"+(pair2.getKey().doubleValue()==pair4.getKey().doubleValue()));
            if (pair2.getKey().doubleValue() == pair3.getKey().doubleValue() && pair2.getKey().doubleValue() == pair4.getKey().doubleValue()) {
                if (pair2.getValue().doubleValue() > pair3.getValue().doubleValue() && pair3.getValue().doubleValue() > pair4.getValue().doubleValue()) {
                    result.remove(i);
                } else if (pair2.getValue().doubleValue() < pair3.getValue().doubleValue() && pair3.getValue().doubleValue() < pair4.getValue().doubleValue()) {
                    result.remove(i);
                } else if (pair2.getValue().doubleValue() >= pair3.getValue().doubleValue() && pair4.getValue().doubleValue() >= pair3.getValue().doubleValue()) {
                    result.remove(i);
                } else if (pair2.getValue().doubleValue() <= pair3.getValue().doubleValue() && pair4.getValue().doubleValue() <= pair3.getValue().doubleValue()) {
                    result.remove(i);
                }
            } else if(pair2.getKey().doubleValue() != pair3.getKey().doubleValue() && pair2.getKey().doubleValue() != pair4.getKey().doubleValue()) {
                if (pair2.getValue().doubleValue() == pair3.getValue().doubleValue() && pair4.getValue().doubleValue() == pair3.getValue().doubleValue()) {
                    result.remove(i);
                }
            }
        }

    }
}
